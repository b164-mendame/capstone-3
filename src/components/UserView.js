import { useState, useEffect } from 'react';
// import courseData from './../data/courses'
import ProductCard from './ProductCard'

export default function UserView({productsProp}) {

	const [productsArr, setProductsArr] = useState([])

	// console.log(courseData)

	//on component mount/page load, useEffect will run and call our fetchData function, which runs our fetch request
	useEffect(() => {
		const products = productsProp.map(product => {
			// console.log(course)
			if(product.isActive){
				return <ProductCard key={product._id} productProp={product} />
			}else{
				return null
			}
		})

		setProductsArr(products)

	}, [productsProp])

	return(
		<>
			{productsArr}
		</>
	)
}


/*

export default function Products() {

	const [products, setProducts] = useState([])
	//console.log(coursesData[0])

	// const courses = coursesData.map(course => {

	// 	return(
	// 		<CourseCard key={course.id} courseProp={course} />
	// 		)
	// })

	useEffect(() => {
		fetch("http://localhost:4000/api/products/")
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setProducts(data.map(product => {
				return(
					<ProductCard key={product._id} productProp={product} />
					)
			}))
		})	
	}, [])

	return(
		<Fragment>
			<h1>Products</h1>
			{products}
		</Fragment>

		)
}

*/
